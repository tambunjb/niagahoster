<?php

class Db {

	private $connection;	

	public function __construct() {
		try {	
			$this->connection = new PDO(getenv('DB_DNS'), getenv('DB_USER'), getenv('DB_PASSWORD'));
		} catch(PDOException $e) {
		    die($e->getMessage());
		}
    }

    public function getAll() {
		try {
		    $stmt = $this->connection->query("SELECT * FROM hosting_package WHERE hosting_package.deleted!=1");

		    $data = $stmt->fetchAll(PDO::FETCH_OBJ);

		    foreach($data as &$d) {
		    	$stmt = $this->connection->query("SELECT * FROM hosting_package_items WHERE hosting_package_items.deleted!=1 AND hosting_package_items.hosting_package_id = $d->hosting_package_id ORDER BY hosting_package_items.order ASC, hosting_package_items.hosting_package_items_id ASC");

		    	$d = (object) array_merge((array)$d, array('hosting_package_items' => $stmt->fetchAll(PDO::FETCH_OBJ)) );
		    }
		    
		    return $data;
		
		} catch(PDOException $e){
		    die($e->getMessage());
		}
	}
}