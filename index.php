<?php

require_once(__DIR__.'/vendor/autoload.php');
require_once(__DIR__ . '/App.php');
require_once(__DIR__ . '/Db.php');


$app = new App(new Twig\Environment(new Twig\Loader\FilesystemLoader(__DIR__.'/templates')));
$app->run();