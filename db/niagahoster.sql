CREATE DATABASE /*!32312 IF NOT EXISTS*/ `niagahoster` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `niagahoster`;

--
-- Table structure for table `hosting_package`
--

DROP TABLE IF EXISTS `hosting_package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hosting_package` (
  `hosting_package_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(125) NOT NULL,
  `price_old` double DEFAULT '0',
  `price` double NOT NULL DEFAULT '0',
  `num_of_users` int(11) DEFAULT '0',
  `discount` int(3) DEFAULT '0',
  `is_best_seller` tinyint(1) DEFAULT '0',
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`hosting_package_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hosting_package`
--

LOCK TABLES `hosting_package` WRITE;
/*!40000 ALTER TABLE `hosting_package` DISABLE KEYS */;
INSERT INTO `hosting_package` VALUES (1,'Bayi',19900,14900,938,0,0,0),(2,'Pelajar',46900,23450,4168,0,0,0),(3,'Personal',58900,38900,10017,0,1,0),(4,'Bisnis',109900,65900,3552,40,0,0);
/*!40000 ALTER TABLE `hosting_package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hosting_package_items`
--

DROP TABLE IF EXISTS `hosting_package_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hosting_package_items` (
  `hosting_package_items_id` int(11) NOT NULL AUTO_INCREMENT,
  `hosting_package_id` int(11) NOT NULL,
  `desc` text,
  `order` int(10) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`hosting_package_items_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hosting_package_items`
--

LOCK TABLES `hosting_package_items` WRITE;
/*!40000 ALTER TABLE `hosting_package_items` DISABLE KEYS */;
INSERT INTO `hosting_package_items` VALUES (1,1,'<b>0.5X RESOURCE POWER</b>',1,0),(2,1,'<b>500 MB</b> Disk Space',2,0),(3,1,'<b>Unlimited</b> Bandwidth',3,0),(4,1,'<b>Unlimited</b> Databases',4,0),(5,1,'<b>1</b> Domain',5,0),(6,1,'<b>Instant</b> Backup',6,0),(7,1,'<b>Unlimited SSL</b> Gratis Selamanya',7,0),(8,2,'<b>1X RESOURCE POWER</b>',1,0),(9,2,'<b>Unlimited</b> Disk Space',2,0),(10,2,'<b>Unlimited</b> Bandwidth',3,0),(11,2,'<b>Unlimited</b> Databases',5,0),(12,2,'<b>10</b> Addon Domains',6,0),(13,2,'<b>Instant</b> Backup',7,0),(14,2,'<b>Domain Gratis</b> Selamanya',8,0),(15,2,'<b>Unlimited SSL</b> Gratis Selamanya',9,0),(16,2,'<b>Unlimited</b> POP3 Email',4,0),(17,3,'<b>2X RESOURCE POWER</b>',1,0),(18,3,'<b>Unlimited</b> Disk Space',2,0),(19,3,'<b>Unlimited</b> Bandwidth',3,0),(20,3,'<b>Unlimited</b> POP3 Email',4,0),(21,3,'<b>Unlimited</b> Databases',5,0),(22,3,'<b>Unlimited</b> Addon Domains',6,0),(23,3,'<b>Instant</b> Backup',7,0),(24,3,'<b>Domain Gratis</b> Selamanya',8,0),(25,3,'<b>Unlimited SSL</b> Gratis Selamanya',9,0),(26,3,'<b>Private</b> Name Server',10,0),(27,3,'<b>SpamAssasin</b> Mail Protection',11,0),(28,4,'<b>3X RESOURCE POWER</b>',1,0),(29,4,'<b>Unlimited</b> Disk Space',2,0),(30,4,'<b>Unlimited</b> Bandwidth',3,0),(31,4,'<b>Unlimited</b> POP3 Email',4,0),(32,4,'<b>Unlimited</b> Databases',5,0),(33,4,'<b>Unlimited</b> Addon Domains',6,0),(34,4,'<b>Magic Auto</b> Backup & Restore',7,0),(35,4,'<b>Domain Gratis</b> Selamanya',8,0),(36,4,'<b>Unlimited SSL</b> Gratis Selamanya',9,0),(37,4,'<b>Private</b> Name Server',10,0),(38,4,'<b>Prioritas</b> Layanan Support<br /><i class=\"fas fa-star text-primary\"></i> <i class=\"fas fa-star text-primary\"></i> <i class=\"fas fa-star text-primary\"></i> <i class=\"fas fa-star text-primary\"></i> <i class=\"fas fa-star text-primary\"></i>',11,0),(39,4,'<b>SpamAssasin</b> Pro Mail Protection',12,0);